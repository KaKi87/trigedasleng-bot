const
    Discord = require('discord-microframework'),
    trig = require('trigedasleng-api');

const config = require('./config');

const
    Bot = new Discord.Bot('Trigedasleng', config.token, 'info'),
    mainCommandHandler = new Discord.CommandHandler('Trigedasleng', config.prefix, null);

const emojis = {
    trig: '<:Trikru:403005227705434112>',
    eng: ':flag_us:'
};

const deleteHandler = new Discord.ReactionHandler('delete', ['🗑']);
const handleDelete = message => {
    deleteHandler.listen(message, [{
        emoji: '🗑',
        action: () => message.delete()
    }], null, false, false, null, null);
};
Bot.use(deleteHandler);

const translationToString = translation => `${translation.before ? `*(${translation.before})* ` : ''}${translation.text}${translation.after ? ` *(${translation.after})*` : ''}`;

const embedSentence = sentence => ({
    title: 'Trigedasleng sentence',
    description: sentence.episode ? `From ${Discord.Utils.twoDigits(sentence.episode.season)}x${Discord.Utils.twoDigits(sentence.episode.number)}` : null,
    fields: [
        { name: 'Original', value: sentence.trig },
        { name: 'Translation', value: sentence.eng },
        { name: 'Leipzig', value: sentence.leipzig },
        { name: 'Etymology', value: sentence.etymology }
    ].concat(sentence.audio ? [ { name: 'Audio', value: sentence.audio } ] : []),
    footer: {
        text: 'trigedasleng.net'
    }
});

mainCommandHandler.subscribe(command => {
    const error = err => {
        if(err !== 'NO_RESULT') console.error(err);
        return command.out(`:x: ${err === 'NO_RESULT' ? 'No result.' : 'Something happened...'}`);
    };
    const displaySentences = sentences => {
        if(!sentences.length) return error('NO_RESULT');
        let
            i = 0/*,
            audioMessage = null*/;
        const
            getSentence = i => embedSentence(sentences[i])/*,
            removeAudio = () => new Promise((resolve, reject) => {
                if(audioMessage) audioMessage.delete().then(() => {
                    audioMessage = null; resolve();
                }).catch(reject); else return resolve();
            }),
            addAudio = i => new Promise((resolve, reject) => {
                if(sentences[i].audio)
                    command.out(sentences[i].audio).then(message => {
                        audioMessage = message;
                        resolve();
                    }).catch(reject);
                else
                    resolve();
            })*/;
        const nav = new Discord.UI.Nav(
            null,
            () => new Promise((resolve/*, reject*/) => /*removeAudio()
                .then(() => addAudio(i > 0 ? --i : i).then(() =>*/ resolve(getSentence(/*i*/i > 0 ? --i : i))
                /*.catch(reject)).catch(reject)).catch(reject)*/),
            () => new Promise((resolve/*, reject*/) => /*removeAudio()
                .then(() => addAudio(i < sentences.length - 1 ? ++i : i).then(() =>*/ resolve(getSentence(/*i*/i < sentences.length - 1 ? ++i : i))
                /*.catch(reject)).catch(reject)).catch(reject)*/),
            null,
            command.author,
            null,
            null,
            null
        );
        command.out(getSentence(i)).then(message => /*addAudio(i).then(() =>*/ nav.messageSent(message).then(reactionHandler => {
            Bot.use(reactionHandler);
            handleDelete(message);
        }/*).catch(error)*/).catch(error)).catch(error);
    };
    switch(command.cmd){
        case 'trig': {
            trig.search(command.args[0], 'trig').then(res => command.out({
                title: 'Trigedasleng ' + emojis.trig,
                description: res.exactMatch.length === 1 ? `[${command.args[0]}](${res.exactMatch[0].link})` : command.args[0],
                fields: res.exactMatch.length === 1 ? [
                    { name: 'Type', value: res.exactMatch[0].types.join('\n'), inline: true },
                    (res.exactMatch[0].etymology ? { name: 'Etymology', value: res.exactMatch[0].etymology.replace(/\[\[(.+)]]/, '[$1](https://trigedasleng.net/word?q=$1)'), inline: true } : false),
                    { name: 'Translations', value: res.exactMatch[0].translations.map(translationToString).join('\n') }
                ].filter(Boolean) : [
                    {
                        name: `${res.words.length > 5 ? 'Multiple' : res.words.length} result${res.words.length > 1 ? 's' : ''} found`,
                        value: (res.exactMatch ? res.exactMatch.map(item => item.text) : [])
                            .concat(res.words.map(item => item.text))
                            .slice(0, 5)
                            .concat(res.words.length > 5 ? [`[${res.words.length - 5} more](${res.link})`] : [])
                            .join('\n')
                    }
                ],
                footer: {
                    text: 'trigedasleng.net'
                }
            }).then(() => displaySentences(res.sentences)).catch(error)).catch(error);
            break;
        }
        case 'eng': {
            trig.search(command.args[0], 'eng').then(res => command.out({
                title: 'English ' + emojis.eng,
                description: command.args[0],
                fields: (res.exactMatch ? res.exactMatch.map(item => ({
                    name: item.text,
                    value: item.translations.map(translationToString).join('\n'),
                    inline: true
                })) : []).concat(res.words ? res.words.map((item, index) => ({
                    name: item.text,
                    value: item.translations.map(translationToString).join('\n'),
                    inline: index > 0
                })) : []),
                footer: {
                    text: 'trigedasleng.net'
                }
            }).then(() => displaySentences(res.sentences)).catch(error)).catch(error);
            break;
        }
        case 'types':
            trig.getTypes().then(data => command.out(
                'Dictionary words types stats :'
                +
                Discord.UI.table([
                    ['TYPE', 'COUNT'],
                    ...Object.entries(data),
                    ['TOTAL', Object.values(data).reduce((a, b) => a + b, 0)]
                ]))
                .catch(error))
                .catch(error);
            break;
        case 'dic':
            trig.getDictionary().then(dic => {
                if(command.args_assoc['type'])
                    dic = dic.filter(word => word.types.includes(command.args_assoc['type']));
                if(command.args_assoc['begin'])
                    dic = dic.slice(dic.findIndex(word => word.text.startsWith(command.args_assoc['begin'])));
                if(command.args_assoc['offset'])
                    dic = dic.slice(command.args_assoc['offset']);
                const
                    maxFieldSize = 15,
                    maxFieldCount = command.args.includes('light') ? 1 : 3,
                    alpha = 'abcdefghijklmnopqrstuvwxyz'.split(''),
                    fields = [],
                    name = letter => Discord.Utils.emojis.alpha[alpha.indexOf(letter)],
                    value = item => `[${item.text}](${item.link})`;
                let
                    lastLetter,
                    n = 0;
                dic.forEach(item => {
                    const
                        letter = item.text[0],
                        field = fields[fields.length - 1];
                    if(lastLetter !== letter || field.value.split('\n').length === maxFieldSize){
                        fields.push({ name: name(letter), value: value(item), inline: true });
                        lastLetter = letter;
                    }
                    else
                        field.value += '\n' + value(item);
                });
                const getCurrentPage = () => {
                    if(n < 0) n = 0;
                    if(n >= fields.length) n = fields.length - 1;
                    return {
                        title: 'Dictionary',
                        description: `${dic.length} words`,
                        fields: fields.slice(n, n + maxFieldCount),
                        footer: {
                            text: 'trigedasleng.net'
                        }
                    };
                };
                const nav = new Discord.UI.Nav(
                    null,
                    () => {
                        n -= maxFieldCount;
                        return Promise.resolve(getCurrentPage());
                    },
                    () => {
                        n += maxFieldCount;
                        return Promise.resolve(getCurrentPage());
                    },
                    null,
                    command.author,
                    null,
                    null,
                    null
                );
                command.out(getCurrentPage()).then(message => nav.messageSent(message).then(reactionHandler => {
                    Bot.use(reactionHandler);
                    handleDelete(message);
                }));
            });
            break;
        case 'translate': {
            const
                sentence = command.args[0].slice(command.args[1].length + 1),
                sourceLang = command.args[1];
            trig.translate(sentence, sourceLang).then(res => command.out({
                title: 'Translation',
                description: `Requested by ${command.author}`,
                fields: [
                    { name: 'Original sentence ' + emojis[sourceLang], value: sentence },
                    { name: 'Translated sentence ' + emojis[sourceLang === 'eng' ? 'trig' : 'eng'], value: res }
                ],
                footer: {
                    text: `trigedasleng.net | Disclaimer : word-by-word translation, accuracy not guaranteed.`
                }
            }).catch(error)).catch(error);
            break;
        }
        case 'help':
            command.out({
                title: 'Help',
                description: `CW The 100's Trigedasleng language documentation bot`,
                fields: [
                    { name: 'Search trigedasleng', value: Discord.Utils.code(`${config.prefix}trig <phrase>`) },
                    { name: 'Search english', value: Discord.Utils.code(`${config.prefix}eng <phrase>`) },
                    { name: 'List words types', value: Discord.Utils.code(`${config.prefix}types`) },
                    { name: 'Browse dictionary', value: Discord.Utils.code(`${config.prefix}dic (light) {type} {begin} {offset}`) },
                    { name: 'Translate', value: Discord.Utils.code(`${config.prefix}translate <trig/eng> <sentence>`) },
                    {
                        name: 'Bot & API wrapper',
                        value: `Designed by ${Discord.Utils.mentionUser('236952680201715714')}
                        [Open source bot](https://git.kaki87.net/KaKi87/trigedasleng-bot)
                        [Open source API wrapper](https://git.kaki87.net/KaKi87/trigedasleng-api)
                        [The 100's Community — Discord](https://discord.gg/658TgTV)`
                    },
                    {
                        name: 'Trigedasleng.net website & database',
                        value: `Designed by ${Discord.Utils.mentionUser('184301886612963328')}
                        [Trigedsleng.net](https://trigedasleng.net/)
                        [Open source website](https://github.com/projectarkadiateam/trigedasleng)
                        [Project Arkadia — Discord](https://discord.gg/Gx7hzZg)`
                    }
                ]
            }).catch(error);
            break;
    }
});

Bot.use(mainCommandHandler);

Bot.start();

process.on('SIGINT', () => Discord.Bot.stopAll().then(() => process.exit()));